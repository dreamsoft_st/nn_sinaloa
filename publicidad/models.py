from django.db import models

from django.db.models.signals import post_delete
from django.dispatch.dispatcher import receiver

class Banner1(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.CharField(max_length=12, blank=True)
	imagen = models.ImageField(upload_to='banners', null=True, blank=True)
	url = models.CharField(max_length=150,blank=True)
	activo = models.BooleanField(default=True)

class Banner2(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.CharField(max_length=12, blank=True)
	imagen = models.ImageField(upload_to='banners', null=True, blank=True)
	url = models.CharField(max_length=150,blank=True)
	activo = models.BooleanField(default=True)

class Banner3(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.CharField(max_length=12, blank=True)
	imagen = models.ImageField(upload_to='banners', null=True, blank=True)
	url = models.CharField(max_length=150,blank=True)
	activo = models.BooleanField(default=True)

class Banner4(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.CharField(max_length=12, blank=True)
	imagen = models.ImageField(upload_to='banners', null=True, blank=True)
	url = models.CharField(max_length=150,blank=True)
	activo = models.BooleanField(default=True)

class Banner5(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.CharField(max_length=12, blank=True)
	imagen = models.ImageField(upload_to='banners', null=True, blank=True)
	url = models.CharField(max_length=150,blank=True)
	activo = models.BooleanField(default=True)

class Banner6(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.CharField(max_length=12, blank=True)
	imagen = models.ImageField(upload_to='banners', null=True, blank=True)
	url = models.CharField(max_length=150,blank=True)
	activo = models.BooleanField(default=True)

@receiver(post_delete, sender=Banner1)
def noticiaimagen_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
@receiver(post_delete, sender=Banner2)
def noticiaimagen_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
@receiver(post_delete, sender=Banner3)
def noticiaimagen_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
@receiver(post_delete, sender=Banner4)
def noticiaimagen_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
@receiver(post_delete, sender=Banner5)
def noticiaimagen_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
@receiver(post_delete, sender=Banner6)
def noticiaimagen_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)