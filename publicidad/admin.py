from django.contrib import admin

from .models import Banner1, Banner2, Banner3, Banner4, Banner5, Banner6

@admin.register(Banner1)
class BannerAdmin1(admin.ModelAdmin):
	list_display = ('nombre', 'telefono', 'url',)
	list_display_links = ('nombre', 'telefono', 'url',)

@admin.register(Banner2)
class BannerAdmin2(admin.ModelAdmin):
	list_display = ('nombre', 'telefono', 'url',)
	list_display_links = ('nombre', 'telefono', 'url',)

@admin.register(Banner3)
class BannerAdmin3(admin.ModelAdmin):
	list_display = ('nombre', 'telefono', 'url',)
	list_display_links = ('nombre', 'telefono', 'url',)

@admin.register(Banner4)
class BannerAdmin4(admin.ModelAdmin):
	list_display = ('nombre', 'telefono', 'url',)
	list_display_links = ('nombre', 'telefono', 'url',)

@admin.register(Banner5)
class BannerAdmin5(admin.ModelAdmin):
	list_display = ('nombre', 'telefono', 'url',)
	list_display_links = ('nombre', 'telefono', 'url',)

@admin.register(Banner6)
class BannerAdmin6(admin.ModelAdmin):
	list_display = ('nombre', 'telefono', 'url',)
	list_display_links = ('nombre', 'telefono', 'url',)