# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publicidad', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner1',
            name='url',
            field=models.CharField(max_length=150, blank=True),
        ),
        migrations.AlterField(
            model_name='banner2',
            name='url',
            field=models.CharField(max_length=150, blank=True),
        ),
        migrations.AlterField(
            model_name='banner3',
            name='url',
            field=models.CharField(max_length=150, blank=True),
        ),
        migrations.AlterField(
            model_name='banner4',
            name='url',
            field=models.CharField(max_length=150, blank=True),
        ),
        migrations.AlterField(
            model_name='banner5',
            name='url',
            field=models.CharField(max_length=150, blank=True),
        ),
        migrations.AlterField(
            model_name='banner6',
            name='url',
            field=models.CharField(max_length=150, blank=True),
        ),
    ]
