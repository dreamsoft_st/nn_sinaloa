"""
Django settings for nnsinaloa project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '1r1teo5x5_4q-7r_erl2-a*68w%pk%_@=_xp&kzmx8f4jo=rcd'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
	'nnsinaloa.apps.MyDjangoSuitConfig',
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',


	'noticia',
	'publicidad',
	'app',
)

MIDDLEWARE_CLASSES = (
	'django.middleware.cache.UpdateCacheMiddleware',

	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',

	'django.middleware.cache.FetchFromCacheMiddleware',
)
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True

ROOT_URLCONF = 'nnsinaloa.urls'

WSGI_APPLICATION = 'nnsinaloa.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'nnsinaloa_db',
		'USER': 'nnsinaloa_db',
		'PASSWORD': 'nnsinaloa_db_password',
		'HOST': 'web444.webfaction.com',

		'OPTIONS': {
			'options': '-c search_path=nnsinaloa'
		}
	}
}
#DATABASES = {
#	'default': {
#		'ENGINE': 'django.db.backends.sqlite3',
#		'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#	}
#}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'America/Mazatlan'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

#STATIC_URL = 'http://nnsinaloa.com/static/'
STATIC_URL = '/static/'
STATICFILES_DIRS = (
	os.path.join(BASE_DIR, 'static'),
)
TEMPLATE_DIRS = (
	os.path.join(BASE_DIR, 'templates'),
)
STATIC_ROOT = '/home/hernandez088/webapps/nnsinaloa_static/'
#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = '/home/hernandez088/webapps/nnsinaloa_media/'

#MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = 'http://nnsinaloa.com/media/'



SUIT_CONFIG = {
	'ADMIN_NAME': 'NN Sinaloa',
	'HEADER_DATE_FORMAT': 'l, j. F Y',
	'HEADER_TIME_FORMAT': 'h:i a',
	'MENU_EXCLUDE': ('auth.group', 'auth'),
}

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
	'django.core.context_processors.request',
	'app.processors.menu'
)

EMAIL_CONTACT = 'direccion@nnsinaloa.com'

EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'nnsinaloa'
EMAIL_HOST_PASSWORD = 'nnsinaloa_hernandez08831'
DEFAULT_FROM_EMAIL = 'sistema@nnsinaloa.com'
SERVER_EMAIL = 'sistema@nnsinaloa.com'