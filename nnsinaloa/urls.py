from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'nnsinaloa.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),

	url(r'^$', 'app.views.home', name='home'),

	url(r'^noticia/([-\w]+)/$', 'noticia.views.noticia_view', name='noticia_view'),
	url(r'^revista/([-\w]+)/$', 'noticia.views.revista_view', name='revista_view'),
    url(r'^seccion/cartones/$', 'noticia.views.list_bycarton_view', name='list_bycarton_view'),
    url(r'^seccion/clasificados/$', 'noticia.views.clasificados', name='clasificados'),
    url(r'^seccion/buzon-ciudadano/$', 'noticia.views.buzon', name='buzonciudadano'),
	url(r'^seccion/([-\w]+)/$', 'noticia.views.list_byseccion_view', name='list_byseccion_view'),
	url(r'^seccion/([-\w]+)/subseccion/([-\w]+)/$', 'noticia.views.list_bysubseccion_view', name='list_bysubseccion_view'),
	url(r'^buscar/', 'noticia.views.buscar', name='buscar'),
	url(r'^nosotros/', 'app.views.nosotros', name='nosotros'),
	url(r'^contacto/', 'app.views.contacto', name='contacto'),


	url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))