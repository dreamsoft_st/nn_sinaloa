from django.db.models import Q
from django.template import RequestContext

from noticia.models import Seccion, Subseccion, Noticia, Ordenseccion, BuzonCiudadano, Revista
from publicidad.models import Banner1,Banner2,Banner3,Banner4, Banner5, Banner6

from datetime import datetime

def menu(request):
	today = datetime.today()
	secciones_index = Ordenseccion.objects.all()[:1]
	secciones_all = Seccion.objects.all().order_by('nombre')
	ultimas_noticias = Noticia.objects.exclude(seccion__slug='opinion').filter(publicar__lte=datetime.now()).order_by('-id')[:15]
	buzon_ult = BuzonCiudadano.objects.order_by('-id').all()[:2]
	revista = Revista.objects.order_by('-id').first()
	banner1 = Banner1.objects.filter(activo=True).order_by('-id').first()
	banner2 = Banner2.objects.filter(activo=True).order_by('-id').first()
	banner3 = Banner3.objects.filter(activo=True).order_by('-id').first()
	banner4 = Banner4.objects.filter(activo=True).order_by('-id').first()
	banner5 = Banner5.objects.filter(activo=True).order_by('-id').first()
	banner6 = Banner6.objects.filter(activo=True).order_by('-id').first()

	datos = {
		'secciones_index' : secciones_index,
		'secciones_all' : secciones_all,
		'ultimas_noticias' : ultimas_noticias,
		'buzon_ult': buzon_ult,
		'banner1' : banner1,
		'banner2' : banner2,
		'banner3' : banner3,
		'banner4' : banner4,
		'banner5' : banner5,
		'banner6' : banner6,
		'revista' : revista,
		}

	return datos