from django.db import models

from django.core.cache import cache
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.sessions.models import Session

@receiver(post_save)
def clear_cache(sender, **kwargs):
	if sender != Session:
		cache.clear()

# Create your models here.
class Contacto(models.Model):
	informacion = models.TextField()

	def __unicode__(self):
		return "%s" % self.pk
	class Meta:
		verbose_name_plural = 'Contacto'

class Nosotros(models.Model):
	informacion = models.TextField()

	def __unicode__(self):
		return "%s" % self.pk
	class Meta:
		verbose_name_plural = 'Nosotros'

