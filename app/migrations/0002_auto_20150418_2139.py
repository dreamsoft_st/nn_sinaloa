# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contacto',
            options={'verbose_name_plural': 'Contacto'},
        ),
        migrations.AlterModelOptions(
            name='nosotros',
            options={'verbose_name_plural': 'Nosotros'},
        ),
    ]
