from django import forms
from django.contrib import admin
from django.db import models
from .models import Contacto, Nosotros
# Register your models here.

@admin.register(Contacto)
class ContactoAdmin(admin.ModelAdmin):
	list_display = ('id', 'Informacion',)
	search_fields = ('informacion',)
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

	def Informacion(self, obj):
		return obj.informacion
	Informacion.allow_tags = True

@admin.register(Nosotros)
class NosotrosAdmin(admin.ModelAdmin):
	list_display = ('id', 'Informacion',)
	search_fields = ('informacion',)

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

	def Informacion(self, obj):
		return obj.informacion
	Informacion.allow_tags = True


