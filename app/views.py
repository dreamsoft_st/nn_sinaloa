from django.shortcuts import render
from django.db.models import Q
from noticia.models import Seccion, Subseccion, Noticia
from datetime import datetime
import operator
from itertools import chain
from .models import Contacto, Nosotros

from django.core.mail import EmailMultiAlternatives, send_mail
from django.conf import settings

bandera = False

def home(request):

	global bandera
	weekday = datetime.now().weekday()
	if weekday == 0 and bandera == False:
		Noticia.objects.all().update(visto = 0)
		bandera = True
	elif weekday != 0 and bandera == True:
		bandera = False


	publicar_hora = datetime.now()
	noticias_slider = Noticia.objects.exclude(seccion__slug='opinion').filter(principal=True, publicar__lte=publicar_hora).order_by('-publicar', '-fecha')[:4]
	noticias_dest = Noticia.objects.exclude(seccion__slug='opinion').filter(publicar__lte=publicar_hora).order_by('-visto', '-fecha')[:6]
	secciones_disp = Seccion.objects#.exclude(slug='opinion')

	ids = {}
	for key in request.COOKIES:
		if 'gusto_' in key:
			value = request.COOKIES.get(key)
			key = int(key.replace('gusto_', ''))
			ids[ str(key) ] = str(value)
	ids = sorted(ids.items(), key=operator.itemgetter(1))
	ids.reverse()
	ids = dict(ids)

	lista = []
	for key in ids.keys()[:4]:
		lista.append( key )

	a = secciones_disp.filter(pk__in=lista)
	if len(ids) < 4:
		total = 4 - len(ids)
		b = secciones_disp.exclude(pk__in=lista)[:total]
		secciones_dest = list(chain(a, b))
	else:
		secciones_dest = a

	data = {
		'noticias_slider' : noticias_slider,
#		'noticias' : noticias,
		'noticias_dest' :noticias_dest,
		'secciones_dest' : secciones_dest
	}

	return render(request, 'index.html', data)

def nosotros(request):
	nosotros = Nosotros.objects.last()
	return render(request, 'nosotros.html', {'nosotros':nosotros})

def contacto(request):

	enviado = 0
	if request.method == 'POST':
		subject = '[Contacto] NN Sinaloa'
		to = settings.EMAIL_CONTACT
		from_email = settings.SERVER_EMAIL
		message = request.POST['InputMessage'] + "<br /><br />Enviado por: <b>" + request.POST['InputName'] + "</b><br />" + request.POST['InputEmail']

		send_mail(subject, message, from_email, [to], html_message=message)
		enviado = 1
		

	contacto = Contacto.objects.last()
	return render(request, 'contacto.html',{'contacto':contacto, 'enviado': enviado})