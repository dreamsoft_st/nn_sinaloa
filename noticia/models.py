from django.db import models
from datetime import datetime, timedelta
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User

from django.db.models.signals import post_delete, post_save
from django.dispatch.dispatcher import receiver

from subprocess import call
import glob, os
from django.core.files import File

class BuzonCiudadano(models.Model):
	descripcion = models.TextField()
	fecha = models.DateField(auto_now_add=True)

	def __unicode__(self):
		return u'%s' % self.descripcion

class ClasificadoSeccion(models.Model):
	nombre = models.CharField(max_length=100)
	slug = models.SlugField(max_length=60, blank=True, editable=False)

	def get_clasificados(self):
		return Clasificado.objects.filter(seccion=self.pk).all()

	def __unicode__(self):
		return u'%s' % self.nombre

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)
		super(ClasificadoSeccion, self).save(*args, **kwargs)

class Clasificado(models.Model):
	seccion = models.ForeignKey(ClasificadoSeccion)
	descripcion = models.TextField()
	fecha = models.DateField(auto_now_add=True)

	def __unicode__(self):
		return u'%s' % self.descripcion

class Cartonista(models.Model):
	nombre = models.CharField(max_length=100, unique=True)

	def __unicode__(self):
		return u'%s' % self.nombre

class Carton(models.Model):
	titulo = models.CharField(max_length=100, unique=True)
	imagen = models.ImageField(upload_to='cartones')
	autor = models.ForeignKey(Cartonista, null=True, blank=True)
	fecha = models.DateField(auto_now_add=True)

	def __unicode__(self):
		return u'%s' % self.titulo

	class Meta:
		verbose_name_plural = 'Cartones'
		

class Seccion(models.Model):
	nombre = models.CharField(max_length=100, unique=True)
	menu_principal = models.BooleanField(default=False)
	slug = models.SlugField(max_length=60, blank=True, editable=False)

	def __unicode__(self):
		return u'%s' % self.nombre

	def get_first(self):
		hora_actual = datetime.now()
		return Noticia.objects.filter(seccion=self.pk, publicar__lte=hora_actual).order_by('-id').first()

	def get_last_noticias(self):
		hora_actual = datetime.now()
		return Noticia.objects.filter(seccion=self.pk, publicar__lte=hora_actual).order_by('-id').all()[1:4]

	def get_subseccion(self):
		return Subseccion.objects.filter(seccion=self.pk).order_by('nombre').all()
		
	class Meta:
		verbose_name_plural = 'Secciones'

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)
		super(Seccion, self).save(*args, **kwargs)

class Subseccion(models.Model):
	seccion = models.ForeignKey(Seccion)
	nombre = models.CharField(max_length=100, unique=True)
	slug = models.SlugField(max_length=60, blank=True, editable=False)
	logo = models.ImageField(upload_to='noticias', null=True, blank=True)

	def __unicode__(self):
		return u'%s' % self.nombre

	class Meta:
		verbose_name_plural = 'Subsecciones'

	def save(self, *args, **kwargs):
		try:
			this = Subseccion.objects.get(pk=self.pk)
			if this.logo != self.logo:
				this.logo.delete(save=False)
		except:
			pass
		self.slug = slugify(self.nombre)
		super(Subseccion, self).save(*args, **kwargs)

class Noticia(models.Model):
	autor = models.ForeignKey(User, editable=False, null=True)
	titulo = models.CharField(max_length=250, unique=True)
	descripcion = models.CharField(max_length=200, blank=True)
	cuerpo = models.TextField()
	imagen = models.ImageField(upload_to='noticias', blank=True, null=True)
	fecha = models.DateTimeField(default=datetime.now)
	seccion = models.ForeignKey(Seccion, blank=True, null=True, editable=False)
	subseccion = models.ForeignKey(Subseccion)
	principal = models.BooleanField(default=False)
	visto = models.PositiveIntegerField(default=0, editable=False)
	slug = models.SlugField(max_length=300, blank=True, editable=False)
	visitas = models.PositiveIntegerField(default=0, editable=False)
	publicar = models.DateTimeField(blank=True, null=True, default=datetime.now)

	def imagenes(self):
		return NoticiaImagen.objects.filter(noticia=self.pk)


	def __unicode__(self):
		return u'%s' % self.titulo

	def save(self, *args, **kwargs):
		try:
			this = Noticia.objects.get(pk=self.pk)
			if this.imagen != self.imagen:
				this.imagen.delete(save=False)
		except:
			pass
		self.seccion = self.subseccion.seccion
		super(Noticia, self).save(*args, **kwargs)
		if not self.slug:
			self.slug = slugify(self.titulo[:50]+str(self.id))
			self.save()
		
		

class NoticiaImagen(models.Model):
	imagen = models.ImageField(upload_to='noticias_imagen')
	noticia = models.ForeignKey(Noticia)
	
	def __unicode__(self):
		return u'%s' % self.noticia.titulo
	
	class meta:
		verbose_name_plural = 'Imagenes'
	def save(self, *args, **kwargs):
		try:
			this = NoticiaImagen.objects.get(pk=self.pk)
			if this.imagen != self.imagen:
				this.imagen.delete(save=False)
		except:
			pass
		super(NoticiaImagen, self).save(*args, **kwargs)

class Ordenseccion(models.Model):
	uno = models.ForeignKey(Seccion, related_name='uno', blank=True, null=True)
	dos = models.ForeignKey(Seccion, related_name='dos', blank=True, null=True)
	tres = models.ForeignKey(Seccion, related_name='tres', blank=True, null=True)
	cuatro = models.ForeignKey(Seccion, related_name='cuatro', blank=True, null=True)
	cinco = models.ForeignKey(Seccion, related_name='cinco', blank=True, null=True)
	seis = models.ForeignKey(Seccion, related_name='seis', blank=True, null=True)
	siete = models.ForeignKey(Seccion, related_name='siete', blank=True, null=True)
	ocho = models.ForeignKey(Seccion, related_name='ocho', blank=True, null=True)
	nueve = models.ForeignKey(Seccion, related_name='nueve', blank=True, null=True)
	diez = models.ForeignKey(Seccion, related_name='diez', blank=True, null=True)
	once = models.ForeignKey(Seccion, related_name='once', blank=True, null=True)

	class Meta:
		verbose_name_plural = 'Orden de Secciones'

class Revista(models.Model):
	ejemplar = models.CharField(max_length=100, unique=True)
	fecha = models.DateField(default=datetime.now)
	slug = models.SlugField(max_length=120, blank=True, editable=False)
	imagen_banner = models.ImageField(upload_to='revista')
	revista_pdf = models.FileField(upload_to='revista_pdf', blank=True, null=True)

	def __unicode__(self):
		return u'%s' % self.ejemplar

	def imagenes(self):
		return RevistaImagen.objects.filter(revista=self.pk).order_by('num_pagina')

	def save(self, *args, **kwargs):
		try:
			this = Revista.objects.get(pk=self.pk)
			if this.imagen_banner != self.imagen_banner:
				this.imagen_banner.delete(save=False)
			if this.revista_pdf != self.revista_pdf:
				this.revista_pdf.delete(save=False)
		except:
			pass
		self.slug = slugify(self.ejemplar)
		super(Revista, self).save(*args, **kwargs)

class RevistaImagen(models.Model):
	num_pagina = models.PositiveIntegerField(default=1)
	imagen = models.ImageField(upload_to='revista_imagen')
	revista = models.ForeignKey(Revista)

	def __unicode__(self):
		return u'%s - %s' % (self.num_pagina, self.revista.ejemplar)

	class Meta:
		verbose_name_plural = 'Imagenes de Revistas'

	def save(self, *args, **kwargs):
		try:
			this = RevistaImagen.objects.get(pk=self.pk)
			if this.imagen != self.imagen:
				this.imagen.delete(save=False)
		except:
			pass
		super(RevistaImagen, self).save(*args, **kwargs)

@receiver(post_save, sender=Revista)
def revistaimagen_save(sender, instance, **kwargs):
	if instance.revista_pdf is not None:
		nombre = 'file' + str(instance.pk)
		pathname = str(os.path.dirname(instance.revista_pdf.path))
		call('convert ' + str(instance.revista_pdf.path) + ' ' + pathname + '/' + str(nombre) + '.png', shell=True)
		index = 1
		
		RevistaImagen.objects.filter(revista=instance.pk).delete()
		for filename in sorted(glob.glob(pathname + '/' + str(nombre) + '-*.png'), key=os.path.getmtime):
			f = File(open(filename))
			rimg = RevistaImagen()
			rimg.num_pagina = index
			rimg.revista = instance
			rimg.imagen.save(str(os.path.basename(filename)), f)
			index = index + 1
			os.remove(filename)
			
@receiver(post_delete, sender=Revista)
def revista_delete(sender, instance, **kwargs):
	if instance.imagen_banner is not None:
		instance.imagen_banner.delete(False)
	if instance.revista_pdf is not None:
		instance.revista_pdf.delete(False)
@receiver(post_delete, sender=RevistaImagen)
def revistaimagen_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
@receiver(post_delete, sender=NoticiaImagen)
def noticiaimagen_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
@receiver(post_delete, sender=Noticia)
def noticia_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
@receiver(post_delete, sender=Subseccion)
def subseccion_delete(sender, instance, **kwargs):
	if instance.logo is not None:
		instance.logo.delete(False)
@receiver(post_delete, sender=Carton)
def subseccion_delete(sender, instance, **kwargs):
	if instance.imagen is not None:
		instance.imagen.delete(False)
		