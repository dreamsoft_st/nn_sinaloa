# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0016_auto_20150611_1436'),
    ]

    operations = [
        migrations.AddField(
            model_name='revista',
            name='revista_pdf',
            field=models.FileField(null=True, upload_to=b'revista_pdf', blank=True),
        ),
    ]
