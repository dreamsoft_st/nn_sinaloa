# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0012_revista_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='revista',
            name='imagen_banner',
            field=models.ImageField(null=True, upload_to=b'revista', blank=True),
        ),
        migrations.AlterField(
            model_name='revista',
            name='ejemplar',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]
