# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0007_clasificado'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClasificadoSeccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='clasificado',
            name='seccion',
            field=models.ForeignKey(default=0, to='noticia.ClasificadoSeccion'),
            preserve_default=False,
        ),
    ]
