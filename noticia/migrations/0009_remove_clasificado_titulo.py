# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0008_auto_20150419_0057'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='clasificado',
            name='titulo',
        ),
    ]
