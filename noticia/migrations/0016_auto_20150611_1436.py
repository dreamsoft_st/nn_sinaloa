# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0015_auto_20150609_1502'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='descripcion',
            field=models.CharField(max_length=200, blank=True),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='slug',
            field=models.SlugField(max_length=300, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='titulo',
            field=models.CharField(unique=True, max_length=250),
        ),
    ]
