# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Carton',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=100)),
                ('imagen', models.ImageField(upload_to=b'cartones')),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Cartones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Cartonista',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='carton',
            name='autor',
            field=models.ForeignKey(to='noticia.Cartonista'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='noticia',
            name='imagen',
            field=models.ImageField(null=True, upload_to=b'noticias', blank=True),
            preserve_default=True,
        ),
    ]
