# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0003_auto_20150330_0233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carton',
            name='fecha',
            field=models.DateField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
