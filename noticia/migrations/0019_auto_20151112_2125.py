# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0018_auto_20151112_2103'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='publicar',
            field=models.DateTimeField(default=datetime.datetime.now, null=True, blank=True),
        ),
    ]
