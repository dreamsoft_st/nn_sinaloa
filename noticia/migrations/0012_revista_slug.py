# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0011_revista_revistaimagen'),
    ]

    operations = [
        migrations.AddField(
            model_name='revista',
            name='slug',
            field=models.SlugField(max_length=120, editable=False, blank=True),
            preserve_default=True,
        ),
    ]
