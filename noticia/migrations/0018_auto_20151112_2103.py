# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0017_revista_revista_pdf'),
    ]

    operations = [
        migrations.AddField(
            model_name='noticia',
            name='publicar',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 13, 4, 3, 39, 938932, tzinfo=utc), null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='revistaimagen',
            name='imagen',
            field=models.ImageField(upload_to=b'revista_imagen'),
        ),
    ]
