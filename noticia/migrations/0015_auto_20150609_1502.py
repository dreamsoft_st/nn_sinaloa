# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0014_noticia_visitas'),
    ]

    operations = [
        migrations.AlterField(
            model_name='revista',
            name='imagen_banner',
            field=models.ImageField(upload_to=b'revista'),
        ),
    ]
