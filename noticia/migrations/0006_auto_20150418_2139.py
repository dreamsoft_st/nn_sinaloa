# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0005_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='carton',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='cartonista',
            name='slug',
        ),
        migrations.AlterField(
            model_name='carton',
            name='titulo',
            field=models.CharField(unique=True, max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cartonista',
            name='nombre',
            field=models.CharField(unique=True, max_length=100),
            preserve_default=True,
        ),
    ]
