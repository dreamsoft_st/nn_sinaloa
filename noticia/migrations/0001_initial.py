# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(unique=True, max_length=100)),
                ('descripcion', models.CharField(max_length=100)),
                ('cuerpo', models.TextField()),
                ('imagen', models.ImageField(upload_to=b'noticias')),
                ('fecha', models.DateTimeField(default=datetime.datetime.now)),
                ('principal', models.BooleanField(default=False)),
                ('visto', models.PositiveIntegerField(default=0, editable=False)),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
                ('autor', models.ForeignKey(editable=False, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NoticiaImagen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imagen', models.ImageField(upload_to=b'noticias_imagen')),
                ('noticia', models.ForeignKey(to='noticia.Noticia')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Ordenseccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name_plural': 'Orden de Secciones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Seccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(unique=True, max_length=100)),
                ('menu_principal', models.BooleanField(default=False)),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Secciones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subseccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(unique=True, max_length=100)),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
                ('logo', models.ImageField(null=True, upload_to=b'noticias', blank=True)),
                ('seccion', models.ForeignKey(to='noticia.Seccion')),
            ],
            options={
                'verbose_name_plural': 'Subsecciones',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='cinco',
            field=models.ForeignKey(related_name='cinco', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='cuatro',
            field=models.ForeignKey(related_name='cuatro', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='diez',
            field=models.ForeignKey(related_name='diez', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='dos',
            field=models.ForeignKey(related_name='dos', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='nueve',
            field=models.ForeignKey(related_name='nueve', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='ocho',
            field=models.ForeignKey(related_name='ocho', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='once',
            field=models.ForeignKey(related_name='once', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='seis',
            field=models.ForeignKey(related_name='seis', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='siete',
            field=models.ForeignKey(related_name='siete', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='tres',
            field=models.ForeignKey(related_name='tres', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ordenseccion',
            name='uno',
            field=models.ForeignKey(related_name='uno', blank=True, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='noticia',
            name='seccion',
            field=models.ForeignKey(blank=True, editable=False, to='noticia.Seccion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='noticia',
            name='subseccion',
            field=models.ForeignKey(to='noticia.Subseccion'),
            preserve_default=True,
        ),
    ]
