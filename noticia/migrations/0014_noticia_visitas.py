# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0013_auto_20150609_1445'),
    ]

    operations = [
        migrations.AddField(
            model_name='noticia',
            name='visitas',
            field=models.PositiveIntegerField(default=0, editable=False),
        ),
    ]
