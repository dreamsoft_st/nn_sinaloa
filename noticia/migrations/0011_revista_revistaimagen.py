# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0010_buzonciudadano'),
    ]

    operations = [
        migrations.CreateModel(
            name='Revista',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ejemplar', models.CharField(max_length=100)),
                ('fecha', models.DateField(default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='RevistaImagen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num_pagina', models.PositiveIntegerField(default=1)),
                ('imagen', models.ImageField(upload_to=b'revista')),
                ('revista', models.ForeignKey(to='noticia.Revista')),
            ],
            options={
                'verbose_name_plural': 'Imagenes de Revistas',
            },
        ),
    ]
