# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0002_auto_20150330_0229'),
    ]

    operations = [
        migrations.AddField(
            model_name='carton',
            name='fecha',
            field=models.DateField(default=datetime.datetime.now, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='carton',
            name='autor',
            field=models.ForeignKey(blank=True, to='noticia.Cartonista', null=True),
            preserve_default=True,
        ),
    ]
