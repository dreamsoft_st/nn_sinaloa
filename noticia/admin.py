from django import forms
from django.contrib import admin
from django.db import models
from .models import Noticia, Seccion, Subseccion, Ordenseccion, NoticiaImagen
from .models import Carton, Cartonista, Clasificado, ClasificadoSeccion, BuzonCiudadano, Revista, RevistaImagen

class NoticiaInline(admin.StackedInline):
	model = NoticiaImagen
	extra = 3
	verbose_name_plural = 'Imagenes'

class RevistaInline(admin.StackedInline):
	model = RevistaImagen
	extra = 1
	verbose_name_plural = 'Imagenes'

@admin.register(Seccion)
class SeccionAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	search_fields = ('nombre',)

@admin.register(Subseccion)
class SubseccionAdmin(admin.ModelAdmin):
	list_display = ('nombre','seccion',)
	search_fields = ('nombre',)
	
@admin.register(Noticia)
class NoticiaAdmin(admin.ModelAdmin):
	list_display=('image_img','autor','titulo','fecha', 'visitas', 'publicar')
	search_fields = ('titulo',)
	list_display_links = ('titulo', 'autor')
	raw_id_fields = ('subseccion',)
	inlines = [ NoticiaInline, ]

	def save_model(self, request, obj, form, change):
		if not change:
			obj.autor = request.user
		obj.save()

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

	def image_img(self, obj):
		return '<img src="/media/%s" width="100">' % obj.imagen
	image_img.short_description = 'Imagen'
	image_img.allow_tags = True



@admin.register(Ordenseccion)
class OrdsecAdmin(admin.ModelAdmin):
	list_display = ('uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve', 'diez', 'once')
	raw_id_fields = ('uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve', 'diez', 'once',)

@admin.register(Carton)
class CartonAdmin(admin.ModelAdmin):
	list_display = ('titulo','autor', 'fecha',)
	search_fields = ('titulo', 'autor', 'fecha',)

@admin.register(Cartonista)
class CartonistaAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	search_fields = ('nombre',)

@admin.register(Clasificado)
class ClasificadoAdmin(admin.ModelAdmin):
	list_display = ('descripcion', 'seccion',)
	search_fields = ('descripcion', 'seccion',)

@admin.register(ClasificadoSeccion)
class ClasificadoSeccionAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	search_fields = ('nombre',)

@admin.register(BuzonCiudadano)
class BuzonCiudadanoAdmin(admin.ModelAdmin):
	list_display = ('descripcion',)
	search_fields = ('descripcion',)

@admin.register(Revista)
class RevistaAdmin(admin.ModelAdmin):
	list_display = ('ejemplar', 'fecha',)
	search_fields = ('ejemplar', 'fecha',)
	inlines = [ RevistaInline, ]