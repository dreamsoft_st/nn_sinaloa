# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from .models import Seccion, Subseccion, Noticia, Carton, Cartonista, ClasificadoSeccion, Clasificado, BuzonCiudadano, Revista
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

import datetime

def noticia_view(request, slug):
	noticia = get_object_or_404(Noticia, slug=slug)
	noticia.visto = noticia.visto + 1
	noticia.visitas = noticia.visitas + 1
	noticia.save()

	response = render(request, 'noticia.html', {'noticia' : noticia, 'TITULO_PAGINA': "- "+noticia.titulo,})

	excluidos = ['opinion', 'cartones', 'buzon-ciudadano', 'clasificados']

	if not noticia.seccion.slug in excluidos:
		key = 'gusto_' + str(noticia.seccion.id)
		if key in request.COOKIES:
			value = int(request.COOKIES[key]) + 2
			response.set_cookie(key, value)
		else:
			response.set_cookie(key, 1)
	return response

def revista_view(request, slug):
	revista = get_object_or_404(Revista, slug=slug)
	return render(request, 'revista.html', {'revista' : revista, 'TITULO_PAGINA': "- Revista "+revista.ejemplar,})
	

def list_bycarton_view(request):
	cartones = Carton.objects.order_by('-id').all()
	page = int(request.GET.get('page', 1))
	paginator = Paginator(cartones, 9)
	try:
		cartones = paginator.page(page)
	except PageNotAnInteger:
		cartones = paginator.page(1)
	except EmptyPage:
		cartones = paginator.page(paginator.num_pages)

	data = {
		'cartones' : cartones,
		'TITULO_PAGINA': "- Cartones",
	}

	return render(request, 'cartonista.html', data)

def list_byseccion_view(request, nombre):
	if nombre == 'opinion':
		return opinion(request)
	seccion = get_object_or_404(Seccion, slug=nombre)
	noticias = Noticia.objects.filter(seccion=seccion, publicar__lte=datetime.datetime.now()).order_by('-id').all()
	page = int(request.GET.get('page', 1))
	paginator = Paginator(noticias, 9)
	try:
		noticias = paginator.page(page)
	except PageNotAnInteger:
		noticias = paginator.page(1)
		page = 1
	except EmptyPage:
		noticias = paginator.page(paginator.num_pages)
		page = paginator.num_pages

	page_numbers = [n for n in range(page - 3, page + 3 + 1) if n > 0 and n <= paginator.num_pages]

	data = {
		'seccion' : seccion,
		'noticias' : noticias,
		'page_numbers': page_numbers,
		'TITULO_PAGINA': "- " + seccion.nombre,
	}

	response = render(request, 'list_noticia.html', data)
	
	excluidos = ['opinion', 'cartones', 'buzon-ciudadano', 'clasificados']

	if not nombre in excluidos:
		key = 'gusto_' + str(seccion.id)
		if key in request.COOKIES:
			value = int(request.COOKIES[key]) + 1
			response.set_cookie(key, value)
		else:
			response.set_cookie(key, 1)

	return response

def list_bysubseccion_view(request, nombre_secc, nombre_sub):
	if nombre_secc == 'opinion':
		return opinion_select(request,nombre_sub)
	seccion = get_object_or_404(Seccion, slug=nombre_secc)
	subseccion = get_object_or_404(Subseccion, seccion = seccion, slug = nombre_sub)
	noticias = Noticia.objects.filter(subseccion=subseccion, publicar__lte=datetime.datetime.now()).order_by('-id').all()

	page = int(request.GET.get('page', 1))
	paginator = Paginator(noticias, 9)
	try:
		noticias = paginator.page(page)
	except PageNotAnInteger:
		noticias = paginator.page(1)
		page = 1
	except EmptyPage:
		noticias = paginator.page(paginator.num_pages)
		page = paginator.num_pages

	page_numbers = [n for n in range(page - 3, page + 3 + 1) if n > 0 and n <= paginator.num_pages]

	data = {
		'seccion' : seccion,
		'subseccion' : subseccion,
		'noticias' : noticias,
		'page_numbers': page_numbers,
		'TITULO_PAGINA': "- " + seccion.nombre,
	}	

	response = render(request, 'list_noticia_sub.html', data)

	excluidos = ['opinion', 'cartones', 'buzon-ciudadano', 'clasificados']

	if not nombre_secc in excluidos:
		key = 'gusto_' + str(seccion.id)
		if key in request.COOKIES:
			value = int(request.COOKIES[key]) + 1
			response.set_cookie(key, value)
		else:
			response.set_cookie(key, 1)

	return response

def buscar(request):
	subseccion = request.GET.get('subseccion', 0)
	seccion = request.GET.get('seccion', 0)
	titulo = request.GET.get('titulo', 0)
	page = int(request.GET.get('page', 1))

	if seccion != 0:
		noticias = Noticia.objects.filter(titulo__icontains=titulo, seccion__slug__icontains=seccion, publicar__lte=datetime.datetime.now()).order_by('-id')
	elif subseccion != 0:
		noticias = Noticia.objects.filter(titulo__icontains=titulo, subseccion__slug__icontains=seccion, publicar__lte=datetime.datetime.now()).order_by('-id')
	else:
		noticias = Noticia.objects.filter(titulo__icontains=titulo, publicar__lte=datetime.datetime.now()).order_by('-id')
	paginator = Paginator(noticias, 9)
	try:
		noticias = paginator.page(page)
	except PageNotAnInteger:
		noticias = paginator.page(1)
		page = 1
	except EmptyPage:
		noticias = paginator.page(paginator.num_pages)
		page = paginator.num_pages

	page_numbers = [n for n in range(page - 3, page + 3 + 1) if n > 0 and n <= paginator.num_pages]

	data = {
		'noticias' : noticias,
		'page_numbers': page_numbers,
		'TITULO_PAGINA': "- Busqueda",
	}

	return render(request, 'buscar.html', data)

def opinion(request):
	page = int(request.GET.get('page', 1))
	subsecciones = Subseccion.objects.filter(seccion__slug='opinion').order_by('nombre').all()
	noticias = Noticia.objects.filter(seccion__slug='opinion', publicar__lte=datetime.datetime.now()).order_by('-id').all()

	paginator = Paginator(noticias, 4)
	try:
		noticias = paginator.page(page)
	except PageNotAnInteger:
		noticias = paginator.page(1)
	except EmptyPage:
		noticias = paginator.page(paginator.num_pages)

	data = {
		'subsecciones' : subsecciones,
		'noticias' : noticias,
		'TITULO_PAGINA': "- Opinion",
	}

	return render(request, 'opinion.html', data) 

def opinion_select(request, nombre_sub):
	subsecciones = Subseccion.objects.filter(seccion__slug='opinion').order_by('nombre').all()
	noticias = Noticia.objects.filter(subseccion__slug__icontains=nombre_sub, publicar__lte=datetime.datetime.now()).order_by('-id').all()
	data = {
		'subsecciones' : subsecciones,
		'noticias' : noticias,
	}
	return render(request, 'opinion.html', data)

def clasificados(request):
	secciones = ClasificadoSeccion.objects.order_by('nombre').order_by('-id').all()
	data = {
		'secciones' : secciones,
		'TITULO_PAGINA': "- Clasificados",
	}
	return render(request, 'clasificados.html', data)

def buzon(request):
	buzon = BuzonCiudadano.objects.order_by('-id').order_by('-id').all()
	page = int(request.GET.get('page', 1))
	paginator = Paginator(buzon, 6)
	try:
		buzon = paginator.page(page)
	except PageNotAnInteger:
		buzon = paginator.page(1)
	except EmptyPage:
		buzon = paginator.page(paginator.num_pages)
	data = {
		'buzon' : buzon,
		'TITULO_PAGINA': "- Buzon Ciudadano",
	}
	return render(request, 'buzon.html', data)
