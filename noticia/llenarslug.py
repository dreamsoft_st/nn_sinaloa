from .models import Seccion, Subseccion
from django.template.defaultfilters import slugify

def llenado():
	secciones = Seccion.objects.all()
	subsecciones = Subseccion.objects.all()

	for subseccion in subsecciones:
		subseccion.slug = slugify(subseccion.nombre)
		subseccion.save()